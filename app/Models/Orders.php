<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    protected $table = 'orders';
    protected $guarded = ['id'];
    protected $fillable = [
        'order_detail', 'user_id', 'address', 'status'
    ];

    protected $casts = [
        'order_detail' => 'json'
    ];

    /**
     * Siparişe ait ürün bilgisini getirir
     * @return \Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson
     */
    public function getProductDetail(){
        //return $this->hasOne('App\Models\Products', 'id');
        return $this->belongsToJson('App\Models\Products', 'order_detail[]->product_id');
    }
}
