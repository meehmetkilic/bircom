<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    protected $table = 'products';
    protected $guarded = ['id'];
    protected $fillable = [
        'name', 'description', 'image', 'price', 'status'
    ];

    /**
     * Ürüne ait stok bilgisini getirir
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getStock(){
        return $this->hasOne('App\Models\ProductStock', 'product_id');
    }
}
