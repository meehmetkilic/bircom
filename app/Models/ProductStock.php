<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $table = 'product_stock';
    protected $guarded = ['id'];
    protected $fillable = [
        'product_id', 'count'
    ];
}
