<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\ProductStock;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Products::paginate(10);
        return view('admin.products.index', [
            'products' => $products
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addForm()
    {
        return view('admin.products.add');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function add(Request $request)
    {
        // verify the data
        $validator = Validator::make($request->all(), [
            'name'          => 'required|unique:products',
            'description'   => 'required',
            'image'         => 'required|mimes:jpg,png',
            'price'         => 'required',
            'stock'         => 'required'
        ]);

        // validation fail check
        if ($validator->fails()) {
            return redirect(route('admin.products.add.form'))->with('error', 'Verify the accuracy of the data you have entered!');
        }

        // file upload
        $fileName = time().'.'.request()->image->getClientOriginalExtension();
        $destinationPath = public_path('upload');
        $request->image->move($destinationPath, $fileName);

        $createProduct = Products::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'image' => url('upload').'/'.$fileName,
            'price' => $request->get('price'),
            'status' => 1
        ]);

        // product count data insert
        $addProductStock = ProductStock::create([
            'product_id'    => $createProduct->id,
            'count'         => $request->get('stock')
        ]);

        if($createProduct){
            return redirect(route('admin.products.add.form'))->with('success', 'Successfully created product!');
        }

        return redirect(route('admin.products.add.form'))->with('error', 'Unfortunately, the product record could not be created.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editForm($id)
    {
        $getProduct = Products::where('id', '=', $id)->first();
        return view('admin.products.edit', [
            'product' => $getProduct
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(Request $request, $id)
    {
        $stockData = $request->get('stock');
        $request->request->remove('_token');
        $request->request->remove('stock');

        $data = $request->all();

        if($request->has('image')){
            // file upload
            $fileName = time().'.'.$request->image->getClientOriginalExtension();
            $destinationPath = public_path('upload');
            $request->image->move($destinationPath, $fileName);
            $data['image'] = url('upload').'/'.$fileName;
        }

        $createProduct = Products::where('id', '=', $id)->update($data);

        if($stockData !== null) {
            $updateProductStock = ProductStock::where('product_id', '=', $id)->update([
                'count' => $stockData
            ]);
        }

        if($createProduct){
            return redirect(route('admin.products.edit.form', $id))->with('success', 'Successfully updated product!');
        }

        return redirect(route('admin.products.add.form', $id))->with('error', 'Unfortunately, the product record could not be updated.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id){
        $getProduct = Products::where('id', '=', $id)->first();
        if($getProduct == null){
            return redirect(route('admin.products'))->with('error', 'Sorry, product not found');
        }

        // product delete
        $deleteProduct = $getProduct->delete();

        if( $deleteProduct ){
            return redirect(route('admin.products'))->with('success', 'Product deleted successfully.');
        }
        else {
            return redirect(route('admin.products'))->with('error', 'Sorry, the product could not be deleted.');
        }
    }

}
