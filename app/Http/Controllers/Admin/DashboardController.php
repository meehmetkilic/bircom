<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Users;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $totalProductCount = Products::count();
        $totalClient = Users::where('role', '=', 'ROLE_USER')->count();
        $totalOrderCount = Orders::count();
        return view('admin.dashboard', [
            'totalProductCount'     => $totalProductCount,
            'totalClientCount'      => $totalClient,
            'totalOrderCount'       => $totalOrderCount
        ]);
    }
}
