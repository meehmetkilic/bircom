<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $getOrders = Orders::paginate(10);
        return view('admin.orders.index', [
            'orders'    => $getOrders
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $getOrder = Orders::where('id', '=', $id)->first();
        return view('admin.orders.detail', [
            'order'    => $getOrder
        ]);
    }

    /**
     * @param $id
     */
    public function confirm($id)
    {
        $updateOrder = Orders::where('id', '=', $id)->update([
            'status'    => 1
        ]);

        if($updateOrder){
            return redirect(route('admin.orders.detail', $id))->with('success', 'Successfully updated order!');
        }

        return redirect(route('admin.orders.detail', $id))->with('error', 'Unfortunately, the order record could not be updated.');
    }
}
