<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard($guard)->check()) {
            $role = Auth::user()->role;

            switch ($role) {
                case 'ROLE_ADMIN':
                    return redirect(route('admin.home'));
                    break;
                case 'ROLE_USER':
                    return redirect(route('user.home'));
                    break;

                default:
                    return redirect(route('login'));
                    break;
            }
        }
        return $next($request);
    }
}
