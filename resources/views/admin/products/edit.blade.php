@extends('layouts.admin')

@section('content')
    <div class="page-heading">
        <h3>Product Management</h3>
    </div>
    <div class="page-content">
        <section class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Product</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ route('admin.products.edit', $product->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Product Name</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="{{ $product->name }}">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Product Description</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <textarea class="form-control"
                                                      name="description" placeholder="Description">{{ $product->description }}</textarea>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Image</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="file" id="image" class="form-control"
                                                   name="image" placeholder="Image" value="{{ $product->image }}">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Price</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="text" id="price" class="form-control"
                                                   name="price" placeholder="Price" value="{{ $product->price }}">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Stock</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="text" id="price" class="form-control"
                                                   name="stock" placeholder="Stock" value="{{ $product->getStock->count }}">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Status</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <select name="status" class="form-control">
                                                <option value="0" @if($product->status == 0) selected @endif>Passive</option>
                                                <option value="1" @if($product->status == 1) selected @endif>Active</option>
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <button type="submit"
                                                    class="btn btn-block btn-warning me-1 mb-1">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
