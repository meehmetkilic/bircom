@extends('layouts.admin')

@section('content')
    <div class="page-heading">
        <h3>Product Management</h3>
    </div>
    <div class="page-content">
        <section class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add New Product</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form form-horizontal" method="POST" action="{{ route('admin.products.add') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Product Name</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="text" id="name" class="form-control"
                                                   name="name" placeholder="Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Product Description</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <textarea class="form-control"
                                                      name="description" placeholder="Description"></textarea>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Image</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="file" id="image" class="form-control"
                                                   name="image" placeholder="Image">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Price</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="text" id="price" class="form-control"
                                                   name="price" placeholder="Price">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Stock</label>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <input type="text" id="stock" class="form-control"
                                                   name="stock" placeholder="Stock">
                                        </div>

                                        <div class="col-md-12">
                                            <button type="submit"
                                                    class="btn btn-block btn-success me-1 mb-1">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
