@extends('layouts.admin')

@section('content')
    <div class="page-heading">
        <h3>Product Management</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    All Products
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td><img src="{{ $product->image }}" width="75" height="75"></td>
                            <td>{{ $product->price }}</td>
                            <td>
                                @if($product->status == 1)
                                    <span class="badge bg-success">Active</span>
                                @else
                                    <span class="badge bg-danger">Passive</span>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group-sm" role="group" aria-label="Basic example">
                                    <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-warning">Edit</a>

                                    <button type="button" class="btn btn-danger VazgecModal" data-bs-toggle="modal"
                                            data-bs-target="#default" data-id="{{ $product->id }}">
                                        Delete
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>

        </section>
    </div>

    <!-- Vazgeç Modal -->
    <div class="modal fade text-left" id="default" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel1">Delete Data</h5>
                    <button type="button" class="close rounded-pill"
                            data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Do you really want to delete the content?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Close</span>
                    </button>
                    <a href="{{ route('admin.products.delete', 1) }}" id="deleteButton" class="btn btn-danger">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <!--Vazgeç modal sonu-->

    @push('script')
        <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script>
        // VazgecModal'ı foreach ile dönmek yerine js ile data-id'i append ediyoruz sil butonuna
        $(document).ready(function() {
            $(document).on("click", ".VazgecModal", function () {
                var productId = $(this).data('id');
                console.log(productId);
                var pastLink =  document.getElementById("deleteButton").getAttribute("href");
                document.getElementById("deleteButton").href = pastLink.slice(0, -1) + productId;
            });
        });
    </script>
    @endpush
@endsection
