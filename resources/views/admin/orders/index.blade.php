@extends('layouts.admin')

@section('content')
    <div class="page-heading">
        <h3>Order Management</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    All Orders
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $key => $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>
                                    @if($order->status == 1)
                                        <span class="badge bg-success">Active</span>
                                    @else
                                        <span class="badge bg-danger">Passive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin.orders.detail', $order->id) }}" class="btn btn-info">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>

        </section>
    </div>

    @push('script')
        <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        <script>
            // VazgecModal'ı foreach ile dönmek yerine js ile data-id'i append ediyoruz sil butonuna
            $(document).ready(function() {
                $(document).on("click", ".VazgecModal", function () {
                    var productId = $(this).data('id');
                    console.log(productId);
                    var pastLink =  document.getElementById("deleteButton").getAttribute("href");
                    document.getElementById("deleteButton").href = pastLink.slice(0, -1) + productId;
                });
            });
        </script>
    @endpush
@endsection
