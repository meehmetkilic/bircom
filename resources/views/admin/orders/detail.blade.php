@extends('layouts.admin')

@section('content')
    <div class="page-heading">
        <h3>Order Management</h3>
    </div>
    <div class="page-content">
        <section class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Order Detail</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Order ID</label>
                                </div>
                                <div class="col-md-10 form-group">
                                    <span>{{ $order->id }}</span>
                                </div>
                                <div class="col-md-2">
                                    <label>Products</label>
                                </div>
                                <div class="col-md-10 form-group">
                                    <table class="table table-striped" id="table1">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Product Name</th>
                                            <th>Image</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->getProductDetail as $key => $cart)
                                            <tr>
                                                <td>{{ $cart->id }}</td>
                                                <td>{{ $cart->name }}</td>
                                                <td><img src="{{ $cart->image }}" width="75" height="75"></td>
                                                <td>{{ $order['order_detail'][$key]['quantity'] }}</td>
                                                <td>{{ $order['order_detail'][$key]['basket_price'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <label>Address</label>
                                </div>
                                <div class="col-md-10 form-group">
                                            <span>{{ $order->address }}</span>
                                </div>

                                <div class="col-md-2">
                                    <label>Status</label>
                                </div>
                                <div class="col-md-10 form-group">
                                    @if($order->status == 1)
                                        <span class="badge bg-success">Approved</span>
                                    @else
                                        <span class="badge bg-danger">Pending</span>
                                    @endif
                                </div>

                                @if($order->status == 0)
                                <div class="col-md-12">
                                    <a href="{{ route('admin.orders.confirm', $order->id) }}"
                                            class="btn btn-block btn-warning me-1 mb-1">Order Confirm</a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
