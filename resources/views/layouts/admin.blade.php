<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('sections.head')
<body>
<div id="app">
    @include('sections.sidebar')
    <div id="main">
        @include('layouts.alert')
        <header class="mb-3">
            <a href="#" class="burger-btn d-block d-xl-none">
                <i class="bi bi-justify fs-3"></i>
            </a>
        </header>

        @yield('content')

        @include('sections.footer')
    </div>
</div>
@include('sections.script')
</body>

</html>
