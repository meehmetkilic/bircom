@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <a href="{{ route('user.basket') }}" class="btn btn-outline-secondary btn-lg" style="margin-bottom: 20px;">Basket</a>
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Welcome, {{ Auth::user()->name_surname }}
                    </div>

                    <div class="container">
                        <div class="row" style="margin-top:20px;margin-bottom: 20px;">
                            @foreach($products as $product)
                            <div class="col-sm">
                                <div class="card" style="width: 18rem; margin-top:20px">
                                    <img src="{{ $product->image }}" width="150" height="150" class="card-img-top" alt="{{ $product->name }}">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $product->name }}</h5>
                                        <p class="card-text">{{ $product->description }}</p>
                                        <p class="card-text">Price : ${{ $product->price }}</p>
                                        <p class="card-text">Stock : {{ $product->getStock->count }}</p>
                                        <p class="btn-holder"><a href="{{ url('/user/add-cart/'.$product->id) }}" class="btn btn-outline-dark btn-block text-center" role="button">Add to cart</a> </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
