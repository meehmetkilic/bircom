<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>2021 &copy; Bircom</p>
        </div>
        <div class="float-end">
            <p>Development <span class="text-danger"><i class="bi bi-heart"></i></span> by <a
                    href="https://www.mehmetkilic.com.tr" target="_blank">Mehmet Kılıç</a></p>
        </div>
    </div>
</footer>
