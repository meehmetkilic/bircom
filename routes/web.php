<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// Admin routes
Route::get('/admin/home', 'Admin\DashboardController@index')->name('admin.home')->middleware('role:ROLE_ADMIN');
Route::get('/admin/products', 'Admin\ProductController@index')->name('admin.products')->middleware('role:ROLE_ADMIN');
Route::get('/admin/products/add', 'Admin\ProductController@addForm')->name('admin.products.add.form')->middleware('role:ROLE_ADMIN');
Route::post('/admin/products/add', 'Admin\ProductController@add')->name('admin.products.add')->middleware('role:ROLE_ADMIN');
Route::get('/admin/products/edit/{id}', 'Admin\ProductController@editForm')->name('admin.products.edit.form')->middleware('role:ROLE_ADMIN');
Route::post('/admin/products/edit/{id}', 'Admin\ProductController@edit')->name('admin.products.edit')->middleware('role:ROLE_ADMIN');
Route::get('/admin/products/delete/{id}', 'Admin\ProductController@delete')->name('admin.products.delete')->middleware('role:ROLE_ADMIN');
Route::get('/admin/orders', 'Admin\OrderController@index')->name('admin.orders')->middleware('role:ROLE_ADMIN');
Route::get('/admin/orders/detail/{id}', 'Admin\OrderController@detail')->name('admin.orders.detail')->middleware('role:ROLE_ADMIN');
Route::get('/admin/orders/confirm/{id}', 'Admin\OrderController@confirm')->name('admin.orders.confirm')->middleware('role:ROLE_ADMIN');




Route::get('/user/home', 'User\DashboardController@index')->name('user.home')->middleware('role:ROLE_USER');
Route::get('/user/basket', 'User\ProductController@basket')->name('user.basket')->middleware('role:ROLE_USER');
Route::any('/user/add-cart/{id}', 'User\ProductController@addToCart')->name('user.add.cart')->middleware('role:ROLE_USER');
Route::patch('/user/update-cart', 'User\ProductController@update')->name('user.update.cart')->middleware('role:ROLE_USER');
Route::delete('user/remove-from-cart', 'User\ProductController@remove')->name('user.remove.cart')->middleware('role:ROLE_USER');
Route::post('user/checkout', 'User\ProductController@checkout')->name('user.checkout')->middleware('role:ROLE_USER');
